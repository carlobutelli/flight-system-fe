import React from 'react'
import { makeStyles, createStyles } from '@material-ui/core/styles'

import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'

import HomeIcon from '@material-ui/icons/Home';
import LocalAirportIcon from '@material-ui/icons/LocalAirport';
import FlightIcon from '@material-ui/icons/Flight';
import TelegramIcon from '@material-ui/icons/Telegram';


const AppMenu: React.FC = () => {
    const classes = useStyles();

    return (
        <List component="nav" className={classes.appMenu} disablePadding>
            <ListItem button component="a" href="/">
                <ListItemIcon className={classes.menuItemIcon}>
                    <HomeIcon />
                </ListItemIcon>
                <ListItemText primary="Simulate" />
            </ListItem>

            <ListItem button component="a" href="/airport">
                <ListItemIcon className={classes.menuItemIcon}>
                    <LocalAirportIcon />
                </ListItemIcon>
                <ListItemText primary="Airport" />
            </ListItem>

            <ListItem button component="a" href="/airline">
                <ListItemIcon className={classes.menuItemIcon}>
                    <TelegramIcon />
                </ListItemIcon>
                <ListItemText primary="Airline" />
            </ListItem>

            <ListItem button component="a" href="/flight">
                <ListItemIcon className={classes.menuItemIcon}>
                    <FlightIcon />
                </ListItemIcon>
                <ListItemText primary="Flight" />
            </ListItem>

        </List>
    )
};

const drawerWidth = 240;

const useStyles = makeStyles(theme =>
    createStyles({
        appMenu: {
            width: '100%',
        },
        navList: {
            width: drawerWidth,
        },
        menuItem: {
            width: drawerWidth,
        },
        menuItemIcon: {
            color: '#97c05c',
        },
    }),
);

export default AppMenu
