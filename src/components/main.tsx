import React from 'react'
import Home from "./Home";
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import Airline from "./Airline";
import Airport from "./Airport";
import Flight from "./Flight";

const Main = () => (
    <Router>
        <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/airline" component={Airline} />
            <Route path="/airport" component={Airport} />
            <Route path="/flight" component={Flight} />
        </Switch>
    </Router>
);

export default Main;
