import React, {Component} from 'react';
import Paper from "@material-ui/core/Paper";
import {Table} from "@material-ui/core";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";

interface AirlineInterface {
    id: number,
    name: string,
    icaoCode: string,
    carrier: string,
    delayedProbability: string,
    cancelledProbability: number
}

class Airline extends Component {
    state = {
        loading: true,
        airlines: Array<AirlineInterface>()
    };

    async componentDidMount() {
        this.getAirlines();
    }

    getAirlines() {
        fetch('https://flight-system-fe.herokuapp.com/airlines')
            .then(response => response.json())
            .then(airlines => {
                this.setState({ airlines: airlines.data, loading: false });
            });
    }

    render () {
        return (
            <Paper className="Airport">
                <div>
                    {this.state.loading || !this.state.airlines ? (
                        <div>loading...</div>
                    ) : (
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Carrier</TableCell>
                                    <TableCell>Icao Code</TableCell>
                                    <TableCell>Name</TableCell>
                                    <TableCell>Delayed Probability</TableCell>
                                    <TableCell>Cancelled Probability</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.airlines.map(airline => (
                                    <TableRow key={airline.id}>
                                        <TableCell>{airline.carrier}</TableCell>
                                        <TableCell>{airline.icaoCode}</TableCell>
                                        <TableCell>{airline.name}</TableCell>
                                        <TableCell>{airline.delayedProbability}</TableCell>
                                        <TableCell>{airline.cancelledProbability}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    )}
                </div>
            </Paper>
        );
    }

}

export default Airline;
