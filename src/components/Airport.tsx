import React, {Component} from 'react';
import Paper from "@material-ui/core/Paper";
import {Table} from "@material-ui/core";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";

interface AirportInterface {
    iataCode: string,
    name: string,
    city: string,
    country: string
}

class Airport extends Component {

    state = {
        loading: true,
        airports: Array<AirportInterface>()
    };

    async componentDidMount() {
        this.getAirports();
    }

    getAirports() {
        fetch('https://flight-system-fe.herokuapp.com/airports')
            .then(response => response.json())
            .then(airports => {
                this.setState({ airports: airports.data, loading: false });
            });
    }

    render () {
        return (
            <Paper className="Airline">
                <div>
                    {this.state.loading || !this.state.airports ? (
                        <div>loading...</div>
                    ) : (
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Iata Code</TableCell>
                                <TableCell>Name</TableCell>
                                <TableCell>City</TableCell>
                                <TableCell>Country</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.state.airports.map(airport => (
                                <TableRow key={airport.iataCode}>
                                    <TableCell>{airport.iataCode}</TableCell>
                                    <TableCell>{airport.name}</TableCell>
                                    <TableCell component="th" scope="row">{airport.city}</TableCell>
                                    <TableCell component="th" scope="row">{airport.country}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                    )}
                </div>
            </Paper>
        );
    }
}


export default Airport;
