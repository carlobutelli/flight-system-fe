import React, {Component} from 'react';
import {Table} from "@material-ui/core";
import TableHead from "@material-ui/core/TableHead";
import Paper from "@material-ui/core/Paper";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";

interface FlightInterface {
    flightNumber: number,
    source: string,
    destination: string,
    scheduledTime: any,
    estimatedTime: string,
    status: string
}

class Flight extends Component {
    state = {
        loading: true,
        flights: Array<FlightInterface>()
    };

    async componentDidMount() {
        this.getFlights();
    }

    getFlights() {
        fetch('https://flight-system-fe.herokuapp.com/flights')
            .then(response => response.json())
            .then(flights => {
                this.setState({ flights: flights.data, loading: false });
            });
    }

    render () {
        return (
            <Paper>
                <div>
                    {this.state.loading || !this.state.flights ? (
                        <div>loading...</div>
                    ) : (
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Flight Number</TableCell>
                                <TableCell>Source</TableCell>
                                <TableCell>Destination</TableCell>
                                <TableCell>Scheduled</TableCell>
                                <TableCell>Estimated</TableCell>
                                <TableCell>Status</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.state.flights.map(flight => (
                                <TableRow key={flight.flightNumber}>
                                    <TableCell>{flight.flightNumber}</TableCell>
                                    <TableCell>{flight.source}</TableCell>
                                    <TableCell component="th" scope="row">{flight.destination}</TableCell>
                                    <TableCell component="th" scope="row">{flight.scheduledTime}</TableCell>
                                    <TableCell component="th" scope="row">{flight.estimatedTime}</TableCell>
                                    <TableCell component="th" scope="row">{flight.status}</TableCell>
                                </TableRow>
                                ))}
                        </TableBody>
                    </Table>
                    )}
                </div>
            </Paper>
        );
    }
}

export default Flight;
