import React, {Component} from 'react';
import {Table} from "@material-ui/core";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Paper from "@material-ui/core/Paper";

// const Home: React.FC = () => {
//     return (
//         <div className="Home">
//             <h1>Home Simulations</h1>
//     </div>
// );
// };
// 2019-12-25T10:50
interface SimulateInterface {
    flightNumber: number,
    source: string,
    destination: string,
    scheduledTime: any,
    estimatedTime: string,
    actualTime: string,
    status: string
}

class Home extends Component {
    state = {
        loading: true,
        arrivals: Array<SimulateInterface>(),
        departures: Array<SimulateInterface>()
    };

    async componentDidMount() {
        this.getSimulations();
    }

    getSimulations() {
        fetch('https://flight-system-fe.herokuapp.com/simulation/FCO/simulate')
            .then(response => response.json())
            .then(response => {
                this.setState({ arrivals: response.arrivals, departures: response.departures, loading: false });
            });
    }

    render() {
        return (
            <Paper>
                <div>
                    {this.state.loading || !this.state.arrivals ? (
                        <div>loading...</div>
                    ) : (
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Flight Number</TableCell>
                                    <TableCell>Source</TableCell>
                                    <TableCell>Destination</TableCell>
                                    <TableCell>Scheduled</TableCell>
                                    <TableCell>Estimated</TableCell>
                                    <TableCell>Status</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.arrivals.map(arrival => (
                                    <TableRow key={arrival.flightNumber}>
                                        <TableCell>{arrival.flightNumber}</TableCell>
                                        <TableCell>{arrival.source}</TableCell>
                                        <TableCell component="th" scope="row">{arrival.destination}</TableCell>
                                        <TableCell component="th" scope="row">{arrival.scheduledTime}</TableCell>
                                        <TableCell component="th" scope="row">{arrival.estimatedTime}</TableCell>
                                        <TableCell component="th" scope="row">{arrival.status}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Flight Number</TableCell>
                                    <TableCell>Source</TableCell>
                                    <TableCell>Destination</TableCell>
                                    <TableCell>Scheduled</TableCell>
                                    <TableCell>Estimated</TableCell>
                                    <TableCell>Status</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.departures.map(departure => (
                                    <TableRow key={departure.flightNumber}>
                                        <TableCell>{departure.flightNumber}</TableCell>
                                        <TableCell>{departure.source}</TableCell>
                                        <TableCell component="th" scope="row">{departure.destination}</TableCell>
                                        <TableCell component="th" scope="row">{departure.scheduledTime}</TableCell>
                                        <TableCell component="th" scope="row">{departure.estimatedTime}</TableCell>
                                        <TableCell component="th" scope="row">{departure.status}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    )}
                </div>
            </Paper>
        )
    }
}
export default Home;
